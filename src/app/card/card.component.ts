import {
  Component,
  OnInit,
  Input,
  HostBinding,
  ViewEncapsulation,
  ViewChild,
  ElementRef
} from '@angular/core';

@Component({
  selector: 'mv-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CardComponent implements OnInit {

  @Input() title: string = '';
  @Input() subtitle: string = '';
  @Input() img: string = '';

  constructor() { }

  ngOnInit() {}

  hasTitle() {
    return this.title.length > 0;
  }

  hasSubtitle() {
    return this.subtitle.length > 0;
  }

  @HostBinding('class.mv-card--no-title')
  get hasNoImageOrTitle() {
    return (this.title.length === 0) && (this.img.length === 0);
  }

  styleTitle() {
    let backgroundImage: string = `url(${this.img})`;
    let style: Object = {};
    if (this.img.length > 0) {
      style = { 'background-image': backgroundImage };
    }
    return style;
  }

}
