import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';
import { FooterComponent } from './footer.component';
import { FooterAboutDirective } from './footer-about.directive';

@NgModule({
  imports: [
    CommonModule,
    CoreModule
  ],
  declarations: [
    FooterComponent,
    FooterAboutDirective,
  ],
  exports: [
    FooterComponent,
    FooterAboutDirective,
  ],
})
export class FooterModule { }
