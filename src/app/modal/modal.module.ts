import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';

import { ModalComponent } from './modal.component';
import { ModalBodyDirective } from './modal-body.directive';
import { ModalFooterDirective } from './modal-footer.directive';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
  ],
  declarations: [
    ModalComponent,
    ModalBodyDirective,
    ModalFooterDirective,
  ],
  exports: [
    ModalComponent,
    ModalBodyDirective,
    ModalFooterDirective,
  ]
})
export class ModalModule { }
