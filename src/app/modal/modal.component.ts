import {
  Component,
  OnInit,
  Input,
  HostBinding,
  HostListener,
  animate,
  trigger,
  state,
  transition,
  style,
  ViewEncapsulation
} from '@angular/core';

@Component({
  selector: 'mv-modal',
  templateUrl: './modal.component.html',
  styleUrls: ['./modal.component.scss'],
  encapsulation: ViewEncapsulation.None,
  animations: [
    trigger('modalAnimation', [
      state('opened', style({ display: 'flex', opacity: 1 })),
      state('closed', style({ display: 'none', opacity: 0 })),
      transition('opened <=> closed', animate(300)),
    ]),
    trigger('modalBoxAnimation', [
      state('opened', style({ transform: 'scale(1)', opacity: 1 })),
      state('closed', style({ transform: 'scale(0)', opacity: 0 })),
      transition('opened <=> closed', animate(300)),
    ]),
    trigger('modalBackdropAnimation', [
      state('show', style({ opacity: 1 })),
      state('hide', style({ opacity: 0 })),
      transition('show <=> hide', animate(500)),
    ]),
  ],
})
export class ModalComponent implements OnInit {

  @Input() title:string = '';
  private visible: Boolean = false;

  @HostBinding('@modalAnimation') get state() {
    return (this.visible) ? 'opened' : 'closed';
  };

  constructor() { }

  ngOnInit() {
  }

  close(){
    this.visible = false;
  }

  open($event) {
    this.visible = true;
  }
}
