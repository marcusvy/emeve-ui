import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutComponent } from './layout.component';
import { GridDirective } from './grid.directive';
import { ContainerDirective } from './container.directive';
import { RowDirective } from './row.directive';
import { ColDirective } from './col.directive';
import { AsideComponent } from './aside/aside.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    LayoutComponent,
    GridDirective,
    ContainerDirective,
    RowDirective,
    ColDirective,
    AsideComponent,
  ],
  exports: [
    LayoutComponent,
    GridDirective,
    ContainerDirective,
    RowDirective,
    ColDirective,
    AsideComponent,
  ]
})
export class LayoutModule { }
