import { Component,
  OnInit,
  Input,
  ViewEncapsulation,
  ViewChild,
  ElementRef,
  Renderer,
} from '@angular/core';

@Component({
  selector: 'mv-control',
  templateUrl: './control.component.html',
  styleUrls: ['./control.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class ControlComponent implements OnInit  {

  @Input() mask:string;
  @Input() formControl;
  @Input() icon:string = '';
  @Input() label:string = '';
  @ViewChild('controlContainer') container:ElementRef;
  @ViewChild('controlAction') controlAction:ElementRef;
  @ViewChild('controlSetup') controlSetup:ElementRef;

  private controlActionHidden:Boolean = true;
  private controlSetupHidden:Boolean = true;

  constructor(private _renderer:Renderer) { }

  ngOnInit() {
    this.controlActionHidden = this.controlAction.nativeElement.childElementCount == 0;
    this.controlSetupHidden = this.controlSetup.nativeElement.childElementCount == 0;
  }

  get hasIcon() {
    return (this.icon.length > 0);
  }

  isControlActionHidden(){
    return this.controlActionHidden;
  }
  isControlSetupHidden(){
    return this.controlSetupHidden;
  }
}
