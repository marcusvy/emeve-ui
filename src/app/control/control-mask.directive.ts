/**
 * https://github.com/pedrolucasp/angular-input-masks/tree/master/src/br
 * https://github.com/the-darc/string-mask/blob/master/src/string-mask.js
 * https://github.com/the-darc/br-masks/tree/master/src
 * https://blog.ngconsultant.io/custom-input-formatting-with-simple-directives-for-angular-2-ec792082976#.r9zh9fnm7
 * http://blog.thoughtram.io/angular/2016/07/27/custom-form-controls-in-angular-2.html
 */
import { Directive } from '@angular/core';

@Directive({
  selector: '[mv-control-mask]'
})
export class ControlMaskDirective {

  constructor() { }

}
