import {Component, Input, OnInit} from '@angular/core';
import {IconService} from "./icon.service";

@Component({
  selector: 'mv-icon',
  templateUrl: './icon.component.html',
  styleUrls: [ './icon.component.scss' ],
})
export class IconComponent implements OnInit {

  @Input() prefix: string;
  @Input() name: string;
  private service:IconService;

  constructor(service:IconService) {
    this.service = service;
  }

  ngOnInit() {
    if(this.prefix===undefined){
      this.prefix = this.service.getPrefix();
    }
  }

  ngClassIcon() {
    return [
      `${this.prefix}`,
      `${this.prefix}-${this.name}`,
    ];
  }

}
