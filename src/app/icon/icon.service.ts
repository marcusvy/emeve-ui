import { Injectable } from '@angular/core';

@Injectable()
export class IconService {

  private prefix: string = 'oi';
  private link: string = '';

  constructor() { }

  getPrefix(){
    return this.prefix;
  }

  setPrefix(value){
    this.prefix = value;
  }

  getLink(){
    return this.link;
  }

  setLink(value){
    this.link = value;
  }
}
