import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';

@Component({
  selector: 'mv-demo-control',
  templateUrl: './demo-control.component.html',
  styleUrls: [ './demo-control.component.scss' ]
})
export class DemoControlComponent implements OnInit {

  demof: FormGroup;

  editorConfig = {
    theme: 'bubble',
    placeholder: "html",
    modules: {
      toolbar: [
        [ 'bold', 'italic', 'underline', 'strike' ],
        [ {'list': 'ordered'}, {'list': 'bullet'} ],
        [ {'header': [ 1, 2, 3, 4, 5, 6, false ]} ],
        [ {'color': []}, {'background': []} ],
        [ {'font': []} ],
        [ {'align': []} ],
        [ 'link', 'image' ],
        [ 'clean' ]
      ]
    }
  };

  constructor(fb: FormBuilder) {
    this.demof = fb.group({
      'user': [ null, Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(10),
      ]) ],
      'email': [ null ],
      'site': [ null ],
      'passwd': [ null ],
      'counter': [ null ],
      'cpf': [ null, Validators.compose([
        Validators.required,
      ]) ],
      'cep': [ null ],
      'cnpj': [ null ],
      'phone': [ null ],
      'editor': [ null ],
      'check': [ null ],
      'telefone': [ null ],
      'data': [ null ],
      'hora': [ null ],
      'select': [ null ],
      'arquivo': [ null ],
    })
  }

  ngOnInit() {
  }

  log($event) {
    console.log($event);
  }

}
