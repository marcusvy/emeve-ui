import { Component, OnInit, ViewChild } from '@angular/core';
import { icons } from './icon';

@Component({
  selector: 'mv-demo-icon',
  templateUrl: './demo-icon.component.html',
  styleUrls: ['./demo-icon.component.scss']
})
export class DemoIconComponent implements OnInit {

  public icons:String[] = [];
  private searchTerm:string = '';

  constructor() { }

  ngOnInit() {
    this.icons = icons;
  }
  filterIcons(event){
    this.searchTerm = event.target.value;
  }
}
