import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'iconfilter',
  pure: true,
})
export class IconPipe implements PipeTransform {

  transform(value: any, args?: any): any {
    return value.filter((item: any) => {
      if (args != '') {
        return item.match(new RegExp('^.*' + args + '*.$'));
      } else {
        return true;
      }
    });
  }

}
