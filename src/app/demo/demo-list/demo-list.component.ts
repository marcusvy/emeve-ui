import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mv-demo-list',
  templateUrl: './demo-list.component.html',
  styleUrls: ['./demo-list.component.scss']
})
export class DemoListComponent implements OnInit {

  public attrWithSeparator = false;

  constructor() { }

  ngOnInit() {
  }

  toggleSeparator() {
    this.attrWithSeparator = (this.attrWithSeparator==null) ? true : null;
  }

}
