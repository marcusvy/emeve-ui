import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MvUiModule } from '../mv-ui/mv-ui.module';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { DemoRoutingModule } from './demo-routing.module';

import { DemoComponent } from './demo.component';
import { DemoMenuComponent } from './demo-menu/demo-menu.component';
import { DemoModalComponent } from './demo-modal/demo-modal.component';
import { DemoListComponent } from './demo-list/demo-list.component';
import { DemoCardComponent } from './demo-card/demo-card.component';
import { DemoIconComponent } from './demo-icon/demo-icon.component';
import { DemoFooterComponent } from './demo-footer/demo-footer.component';
import { DemoHeaderComponent } from './demo-header/demo-header.component';
import { DemoToolbarComponent } from './demo-toolbar/demo-toolbar.component';
import { DemoBadgeComponent } from './demo-badge/demo-badge.component';
import { DemoButtonComponent } from './demo-button/demo-button.component';
import { DemoColorComponent } from './demo-color/demo-color.component';
import { DemoTypographyComponent } from './demo-typography/demo-typography.component';
import { DemoAvatarComponent } from './demo-avatar/demo-avatar.component';
import { DemoDashboardComponent } from './demo-dashboard/demo-dashboard.component';
import { DemoControlComponent } from './demo-control/demo-control.component';
import { IconPipe } from './demo-icon/icon.pipe';
import { DemoPipesComponent } from './demo-pipes/demo-pipes.component';
import { DemoLayoutComponent } from './demo-layout/demo-layout.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    DemoRoutingModule,
    MvUiModule,
  ],
  declarations: [
    DemoComponent,
    DemoMenuComponent,
    DemoModalComponent,
    DemoListComponent,
    DemoCardComponent,
    DemoIconComponent,
    DemoAvatarComponent,
    DemoFooterComponent,
    DemoHeaderComponent,
    DemoToolbarComponent,
    DemoBadgeComponent,
    DemoButtonComponent,
    DemoColorComponent,
    DemoTypographyComponent,
    DemoDashboardComponent,
    DemoControlComponent,
    IconPipe,
    DemoPipesComponent,
    DemoLayoutComponent,
  ]
})
export class DemoModule { }
