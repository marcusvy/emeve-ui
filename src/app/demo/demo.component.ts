import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'mv-ui-demo',
  templateUrl: './demo.component.html',
  styleUrls: ['./demo.component.scss']
})
export class DemoComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  onClickButton() {
    console.log("Botão clicou!");
  }
}
