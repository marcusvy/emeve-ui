import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DemoComponent } from './demo.component';
import { DemoDashboardComponent } from './demo-dashboard/demo-dashboard.component';
import { DemoControlComponent } from './demo-control/demo-control.component';
import { DemoIconComponent } from './demo-icon/demo-icon.component';

import { DemoMenuComponent } from './demo-menu/demo-menu.component';
import { DemoModalComponent } from './demo-modal/demo-modal.component';
import { DemoListComponent } from './demo-list/demo-list.component';
import { DemoCardComponent } from './demo-card/demo-card.component';
import { DemoFooterComponent } from './demo-footer/demo-footer.component';
import { DemoHeaderComponent } from './demo-header/demo-header.component';
import { DemoToolbarComponent } from './demo-toolbar/demo-toolbar.component';
import { DemoBadgeComponent } from './demo-badge/demo-badge.component';
import { DemoButtonComponent } from './demo-button/demo-button.component';
import { DemoColorComponent } from './demo-color/demo-color.component';
import { DemoTypographyComponent } from './demo-typography/demo-typography.component';
import { DemoAvatarComponent } from './demo-avatar/demo-avatar.component';
import { DemoLayoutComponent } from './demo-layout/demo-layout.component';

import { DemoPipesComponent } from './demo-pipes/demo-pipes.component';
const routes: Routes = [
  { path: 'demo',
    component: DemoComponent,
    children: [
      { path: '', component: DemoDashboardComponent},
      { path: 'pipes', component: DemoPipesComponent },
      { path: 'avatar', component: DemoAvatarComponent },
      { path: 'badge', component: DemoBadgeComponent },
      { path: 'button', component: DemoButtonComponent },
      { path: 'card', component: DemoCardComponent },
      { path: 'color', component: DemoColorComponent },
      { path: 'control', component: DemoControlComponent },
      { path: 'footer', component: DemoFooterComponent },
      { path: 'header', component: DemoHeaderComponent },
      { path: 'icon', component: DemoIconComponent },
      { path: 'list', component: DemoListComponent },
      { path: 'menu', component: DemoMenuComponent },
      { path: 'modal', component: DemoModalComponent },
      { path: 'toolbar', component: DemoToolbarComponent },
      { path: 'typography', component: DemoTypographyComponent },
      { path: 'layout', component: DemoLayoutComponent },
    ],
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [],
})
export class DemoRoutingModule {
}
