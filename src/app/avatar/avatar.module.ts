import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AvatarComponent } from './avatar.component';
import { AvatarShieldComponent } from './avatar-shield/avatar-shield.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [
    AvatarComponent,
    AvatarShieldComponent,
  ],
  exports: [
    AvatarComponent,
    AvatarShieldComponent,
  ],
})
export class AvatarModule { }
