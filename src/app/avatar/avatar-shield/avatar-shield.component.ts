import { Component, Input } from '@angular/core';

@Component({
  selector: 'mv-avatar-shield',
  templateUrl: './avatar-shield.component.html',
  styleUrls: ['./avatar-shield.component.scss']
})
export class AvatarShieldComponent {

  @Input() title: string = '';
  @Input() image: string = '';

  constructor() { }

}
