import { Component, OnInit, Input, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mv-avatar',
  templateUrl: './avatar.component.html',
  styleUrls: ['./avatar.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AvatarComponent implements OnInit {

  @Input() src: string = '';

  constructor() { }

  ngOnInit() {
  }

}
