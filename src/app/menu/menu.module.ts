import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CoreModule } from '../core/core.module';

import { MenuComponent } from './menu.component';
import { MenuItemComponent } from './menu-item/menu-item.component';
import { MenuListComponent } from './menu-list/menu-list.component';
import { MenuHeaderDirective } from './menu-header.directive';
import { MenuDivisorDirective } from './menu-divisor.directive';
import { MenuButtonDirective } from './menu-button.directive';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
  ],
  declarations: [
    MenuComponent,
    MenuItemComponent,
    MenuDivisorDirective,
    MenuHeaderDirective,
    MenuButtonDirective,
    MenuListComponent
  ],
  exports: [
    MenuComponent,
    MenuItemComponent,
    MenuDivisorDirective,
    MenuHeaderDirective,
    MenuButtonDirective,
    MenuListComponent,
  ],
})
export class MenuModule { }
