import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mv-menu-list',
  templateUrl: './menu-list.component.html',
  styleUrls: ['./menu-list.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MenuListComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
