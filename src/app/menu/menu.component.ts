import {
  Component,
  OnInit,
  ViewChildren,
  ViewEncapsulation
} from '@angular/core';

@Component({
  selector: 'mv-menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MenuComponent implements OnInit {

  private visible: Boolean = false;
  @ViewChildren('mv-menu-item') private items;

  constructor() { }

  ngOnInit() {

  }
  // Close on click in buttons but wait 300ms
  onBlur() {
    setTimeout(()=>{
      this.close();
    }, 300);
  }
  onMouseLeaves() {
    this.close();
  }

  classMenuContainer() {
    let style = {
      'js-mv-menu--visible': this.visible
    };
    return style;
  }

  open(): void {
    this.visible = true;
  }

  close(): void {
    this.visible = false;
  }

  toggle(): Boolean {
    this.visible = !this.visible;
    return this.visible;
  }

}
