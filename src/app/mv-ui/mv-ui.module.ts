import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

/**
 * The component modules to share
 */
import {CoreModule} from '../core/core.module';

import {AvatarModule} from '../avatar/avatar.module';
import {BadgeModule} from '../badge/badge.module';
import {ButtonModule} from '../button/button.module';
import {CardModule} from '../card/card.module';
import {ControlModule} from '../control/control.module';
import {FooterModule} from '../footer/footer.module';
import {HeaderModule} from '../header/header.module';
import {IconModule} from '../icon/icon.module';
import {LayoutModule} from '../layout/layout.module';
import {ListModule} from '../list/list.module';
import {MenuModule} from '../menu/menu.module';
import {ModalModule} from '../modal/modal.module';
import {ToolbarModule} from '../toolbar/toolbar.module';

import {MvUiComponent} from './mv-ui.component';

@NgModule({
  imports: [
    CommonModule,
    CoreModule,
    FormsModule,
    ReactiveFormsModule,
    // AvatarModule,
    // BadgeModule,
    // ButtonModule,
    // CardModule,
    // FooterModule,
    // HeaderModule,
    // IconModule,
    LayoutModule,
    // ListModule,
    // MenuModule,
    // ModalModule,
    // ToolbarModule,
  ],
  exports: [
    MvUiComponent,
    CoreModule,
    AvatarModule,
    BadgeModule,
    ButtonModule,
    CardModule,
    ControlModule,
    FooterModule,
    HeaderModule,
    IconModule,
    LayoutModule,
    ListModule,
    MenuModule,
    ModalModule,
    ToolbarModule,
  ],
  declarations: [ MvUiComponent ]
})
export class MvUiModule {
}
