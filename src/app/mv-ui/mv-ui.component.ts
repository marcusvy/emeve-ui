import { Component, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mv-ui',
  templateUrl: './mv-ui.component.html',
  styleUrls: ['./mv-ui.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class MvUiComponent {}
