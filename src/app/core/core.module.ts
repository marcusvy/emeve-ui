import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ButtonModule } from '../button/button.module';
import { IconModule } from '../icon/icon.module';
import { LayoutModule } from '../layout/layout.module';
import { CoreComponent } from './core.component';
import { CpfPipe } from './pipe/cpf.pipe';
import { PhonePipe } from './pipe/phone.pipe';
import { BoletoPipe } from './pipe/boleto.pipe';
import { CepPipe } from './pipe/cep.pipe';
import { CnpjPipe } from './pipe/cnpj.pipe';

@NgModule({
  imports: [
    CommonModule,
    ButtonModule,
    IconModule,
    LayoutModule,
  ],
  declarations: [
    CoreComponent,
    CpfPipe,
    PhonePipe,
    BoletoPipe,
    CepPipe,
    CnpjPipe,
  ],
  exports: [
    CoreComponent,
    ButtonModule,
    IconModule,
    LayoutModule,
    CpfPipe,
    PhonePipe,
    BoletoPipe,
    CepPipe,
    CnpjPipe,
  ]
})
export class CoreModule { }
