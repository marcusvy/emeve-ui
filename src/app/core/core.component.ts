import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'mv-core',
  template: '',
  styleUrls: ['./core.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class CoreComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
