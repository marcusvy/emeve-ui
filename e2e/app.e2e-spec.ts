import { EmeveUiPage } from './app.po';

describe('emeve-ui App', function() {
  let page: EmeveUiPage;

  beforeEach(() => {
    page = new EmeveUiPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});
